#! /usr/bin/env python3

import glob
import os
import string
import PTN
import tmdbsimple as tmdb
from fuzzywuzzy import fuzz
import enzyme
import urllib.request
import subprocess
import platform

mkvpropedit_path = "PATH TO MKVPROPEDIT"

tmdb.API_KEY = "YOUR TMDB API KEY"
tmdb = tmdb.Search()


def tmdb_search(guess):
    movies = []
    for movie in tmdb.movie(query=guess["title"])["results"]:
        try:
            year = int(movie["release_date"][:4])
            movie["year"] = year
        except:
            continue
        movies.append(movie)
    return movies


def guess_movie(filename, retry=False):
    guess = PTN.parse(filename)
    movies = tmdb_search(guess)

    if "year" in guess:
        year = int(guess["year"])
        movies = [m for m in movies if m["year"] == year]

    ratio = 0
    best = None
    for m in movies:
        r = fuzz.ratio(guess["title"], m["title"]) 
        if r > ratio:
            ratio = r
            best = m

    if best is None and not retry:
        print("Couldn't guess the movie for '{}'".format(guess["title"]))
        title = input("Please type the title: ")
        if title != "":
            return guess_movie(title, True)

    return best


def get_resolution_name(mkv):
    width = mkv.video_tracks[0].width
    height = mkv.video_tracks[0].height

    if width > 1800 or height > 1000:
        return "1080p"
    elif width > 1000 or height > 700:
        return "720p"
    else:
        return "SD"


def get_track_language(track):
    lang = track.language
    if lang in ("unk", "und", "xxx"):
        lang = None
    return lang


def get_language_tag(mkv):
    audio = [get_track_language(t) for t in mkv.audio_tracks]

    if len(audio) > 1:
        return "MULTI"
    if audio[0] and audio[0][:2] == "fr":
        return "VF"

    video = [get_track_language(t) for t in mkv.video_tracks]
    sub = [get_track_language(t) for t in mkv.subtitle_tracks]

    lang = "VO"

    if len(sub) != 0 or any(l is not None for l in video):
        lang += "ST"
        if any(l and l[:2] == "fr" for l in sub + video):
            lang += "FR"
        elif any(l and l[:2] == "en" for l in sub + video):
            lang += "A"

    return lang


def check_title(title):
    ok = string.printable + "àâäçéèêëîïôöùûü"
    if any(c not in ok for c in title):
        print("Unusual characters found in", title)
        new = input("Type a replacement or skip with ENTER: ")
        if new != "":
            title = new
    if "Windows" in platform.platform():
        wrong = "<>:\"/\\|?*"
        if any(c in wrong for c in title):
            print("Found invalid character for windows in ", title)
            new = input("Type a replacement or skip with ENTER: ")
            if new != "":
                title = new
    return title


def get_poster(movie):
    url = "https://image.tmdb.org/t/p/original" + movie["poster_path"]
    urllib.request.urlretrieve(url, "cover.jpg")


def mkv_edit(fname, movie, title):
    get_poster(movie)
    command = [mkvpropedit_path, fname, "--edit", "info",
               "--set", "title=" + title,
               "--attachment-name", "cover.jpg",
               "--add-attachment", "cover.jpg"]
    try:
        ret = subprocess.check_call(command,
                stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    except CalledProcessError as e:
        print("Error:", e.output)
    os.remove("cover.jpg")


for fname in glob.glob("*.mkv"):
    guess = guess_movie(fname)
    if guess is None:
        print(fname, ": movie not found.")
        continue
    title = check_title(guess["original_title"])
    year = guess["year"]
    new_name = ""

    with open(fname, 'rb') as f:
        mkv = enzyme.MKV(f)
        lang = get_language_tag(mkv)
        res = get_resolution_name(mkv)
        new_name = "{} ({}) {} {}.mkv".format(title, year, lang, res)
    
    print(fname, '->', new_name)
    mkv_edit(fname, guess, title)
    os.rename(fname, new_name)
