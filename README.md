mkvtag
======

For all mkv movies in the curent directory:

* Parses the file name according to typical torrent naming schemes (PTN)
* Searches for the best match on The Movie Database (TMDB)
* Infers the language and resolution categories from the MKV metadata
* Renames the file as "TITLE (YEAR) LANG RES"
* Adds the movie's poster as cover attachment

This project was made to fit my needs out of the box, you will probably need to
hack it slightly for yours.

### Setup

* Get python 3.X and pip
* Install requirements first with ```pip install -r requirements.txt```
* Get mkvpropedit
* Get an API key for The Movie Database
* Fill the two config strings in mkvtag.py with the previous two items
* ???
* Profit.
